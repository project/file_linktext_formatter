<?php

namespace Drupal\file_linktext_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;
/**
 * Plugin implementation of the 'file_fieldtext' formatter.
 *
 * @FieldFormatter(
 *   id = "file_fieldtext",
 *   label = @Translation("Link text from field"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class FieldTextFileFormatter extends FileFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings = parent::defaultSettings();

    $settings['use_field_as_link_text'] = 0;

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $entityFieldManager = \Drupal::service('entity_field.manager');
    $fields = $entityFieldManager->getFieldDefinitions($this->fieldDefinition->getTargetEntityTypeId(), $this->fieldDefinition->getTargetBundle());
    $options = [];
    /* @var \Drupal\Core\Field\BaseFieldDefinition $field */
    foreach ($fields as $field) {
      $options[0] = $this->t('Disabled');
      if ($field->getType() == 'string') {
        $options[$field->getName()] = $field->getLabel();
      }
    }
    if (!empty($options)) {
      $form['use_field_as_link_text'] = [
        '#type' => 'select',
        '#title' => $this->t('Use field value as link text'),
        '#options' => $options,
        '#default_value' => $this->getSetting('use_field_as_link_text')
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    if ($this->getSetting('use_field_as_link_text')) {
      $fieldname = $this->getSetting('use_field_as_link_text');
      $entityFieldManager = \Drupal::service('entity_field.manager');
      $fields = $entityFieldManager->getFieldDefinitions($this->fieldDefinition->getTargetEntityTypeId(), $this->fieldDefinition->getTargetBundle());
      /* @var \Drupal\Core\Field\BaseFieldDefinition $field */
      $field = $fields[$fieldname];
      $summary[] = $this->t('Use field value as link text: @field', ['@field' => $field->getLabel()]);
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $entity = $items->getEntity();
    $fieldname = $this->getSetting('use_field_as_link_text');
    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $item = $file->_referringItem;
      $elements[$delta] = [
        '#theme' => 'file_link',
        '#file' => $file,
        '#description' => $fieldname ? $entity->{$fieldname}->value : NULL,
        '#cache' => [
          'tags' => $file->getCacheTags(),
        ],
      ];
      // Pass field item attributes to the theme function.
      if (isset($item->_attributes)) {
        $elements[$delta] += ['#attributes' => []];
        $elements[$delta]['#attributes'] += $item->_attributes;
        // Unset field item attributes since they have been included in the
        // formatter output and should not be rendered in the field template.
        unset($item->_attributes);
      }
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getFieldStorageDefinition()->getCardinality() == 1;
  }

}