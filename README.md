## Introduction
This module allows to use string text field value as link text on a file display formatter.

This is similar to generic file formatter from core, but allows to use the value of an arbitrary field (string field only) for the link text.

## Requirements
This module has no additional requirements

## Installation
Install this module as usual

## Configuration
Configuration is done on field basis (field formatter). Go to Manage Display tab of your entity and change the display formatter for your file field.